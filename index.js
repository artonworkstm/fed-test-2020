(async () => {
  let sortBy = document.querySelector(".sort-by"),
    filter = document.querySelector(".filter-by"),
    filterClose = document.querySelector(".filter-close"),
    clearAll = document.querySelector(".clear-all"),
    filters = document.querySelectorAll(".filters input"),
    boxes = Array.from(filters),
    viewItems = document.querySelector(".view-items"),
    buyNow = document.querySelector(".buy-now"),
    columnToggleLabel = document.querySelector(".column-toggle"),
    columnToggle = document.querySelector("input[name=column-toggle]"),
    windowWidth = window.innerWidth,
    items = document.querySelector(".items"),
    results = document.querySelector(".result-number"),
    highSortedArray = [],
    lowSortedArray = [],
    filteredArray = [],
    multiple = [];

  /**---------------------------------------------------    EVENT LISTENERS   ---------------------------------------------------*/

  // SAVE OPTIONS TO SESSION STORAGE IF PAGE RELOADS
  window.addEventListener("unload", () => {
    const options = {
      column: columnToggle.checked ? "checked" : "unchecked",
      sort: sortBy.value,
      multiple: multiple,
      filteredArray: filteredArray,
    };

    boxes.map((item) => {
      options[item.id] = item.checked ? "checked" : "unchecked";
    });

    sessionStorage.setItem("options", JSON.stringify(options));
  });

  window.addEventListener("resize", (e) => {
    e.preventDefault();
    if (window.innerWidth < 700) {
      windowWidth = window.innerWidth;
      columnToggleLabel.style.display = "block";
      changeStyle(columnToggle.checked);
    } else {
      windowWidth = window.innerWidth;
      columnToggleLabel.style.display = "none";
      changeStyle("desktop");
    }
  });

  // SORT OPTIONS
  sortBy.addEventListener("change", () => {
    multiple.length !== 0
      ? priceSorter(filteredArray)
      : priceSorter(productList);
  });

  // FILTER OPTIONS
  filter.addEventListener("click", () => {
    const filters = document.querySelector(".darken-filter");

    filters.style.display = "block";
  });

  // FILTER OPTIONS CLOSE
  filterClose.addEventListener("click", () => {
    const filters = document.querySelector(".darken-filter");

    filters.style.display = "none";
  });

  // FILTER OPTIONS CLOSE
  viewItems.addEventListener("click", () => {
    const filters = document.querySelector(".darken-filter");

    filters.style.display = "none";
  });

  // UNCHECK ALL FILTERS
  clearAll.addEventListener("click", () => {
    boxes.map((item) => (item.checked = false));
    multiple = [];
    filteredArray = [];

    priceSorter(productList);
    updateResults();
  });

  // FILTER
  boxes.map((item, index) => {
    item.addEventListener("change", (e) => {
      if (item.checked) {
        multiple.push(item.className + " ");

        filterResults(item.className);
        priceSorter(filteredArray);
      } else {
        multiple = multiple.filter((e) => e !== item.className + " ");

        filteredArray = [];
        multiple.map((item) => filterResults(item.trim()));

        multiple.length !== 0
          ? priceSorter(filteredArray)
          : priceSorter(productList);
      }

      updateResults();
    });
  });

  // CHANGES COLUMNS
  columnToggle.addEventListener("change", () => {
    if (columnToggleLabel.style.display === "none") {
      changeStyle("desktop");
    } else {
      changeStyle(columnToggle.checked);
    }
  });

  /**---------------------------------------------------    FUNCTIONS   ---------------------------------------------------*/

  // SORTING ITEMS BY PRICE
  const priceSorter = (list) => {
    // CLEAR ITEMS TAG
    items.textContent = "";

    // IF LOW
    if (sortBy.value === "low") {
      lowSortedArray = Array.from(list);
      lowSortedArray.sort(({ price: a }, { price: b }) => {
        return a - b;
      });

      lowSortedArray.map((item, index) => {
        createCards(items, item, index);
      });

      // IF HIGH
    } else if (sortBy.value === "high") {
      highSortedArray = Array.from(list);
      highSortedArray.sort(({ price: a }, { price: b }) => {
        return b - a;
      });

      highSortedArray.map((item, index) => {
        createCards(items, item, index);
      });

      //IF NORMAL
    } else if (sortBy.value === "normal") {
      const normalArray = Array.from(list);
      normalArray.map((item, index) => {
        createCards(items, item, index);
      });
    }
  };

  // CREATES CARD OF PRODUCT
  const createCards = (items, item, index) => {
    const div = document.createElement("div");

    div.onclick = () => {
      const descriptionContainer = document.querySelector(".darken");
      const description = document.querySelector(".description");
      const close = document.querySelector(".close");

      descriptionContainer.style.display = "block";
      description.innerHTML = item.description;
      buyNow.onclick = () => window.open(item.link, "__blank", "noopener"); // Noopener prevents security issues when opening __blank

      close.addEventListener(
        "click",
        () => (descriptionContainer.style.display = "none")
      );
    };

    div.className = "product";
    div.innerHTML = `<img class="product-image" src="${item.image_link}" alt="Image not found"/><span class="title">${item.title}</span><section class="prices"><span class="crossed-price">$${item.crossed_price}</span><span class="price">$${item.price}</span></section>`;

    items.appendChild(div);
  };

  // CHANGE STYLE SO ITEMS WILL FIT
  const changeStyle = (bool) => {
    const button = document.querySelector(".column-toggle");
    const product = Array.from(document.querySelectorAll(".product"));
    const images = Array.from(document.querySelectorAll(".product-image"));

    if (bool === true) {
      button.innerHTML = `<i class="fas fa-border-all"></i>`;
      items.style.gridTemplateColumns = "100%";

      product.map((item) => {
        item.style.width = "90%";
        item.style.height = "120vw";
        item.style.padding = "5%";
      });

      images.map((item) => {
        item.style.height = "80%";
      });
    } else if (bool === "desktop") {
      items.style.gridTemplateColumns = "25% 25% 25% 25%";

      product.map((item) => {
        item.style.width = "20vw";
        item.style.height = "30vw";
        item.style.padding = "10%";
      });

      images.map((item) => {
        item.style.height = "70%";
      });
    } else {
      button.innerHTML = `<i class="far fa-square">`;
      items.style.gridTemplateColumns = "50% 50%";

      product.map((item) => {
        item.style.width = "40vw";
        item.style.height = "60vw";
        item.style.padding = "10%";
      });

      images.map((item) => {
        item.style.height = "70%";
      });
    }
  };

  // PUSH FILTERED PRODUCTLIST TO FILTEREDARRAY
  const filterResults = (filter) => {
    productList.map((item) => {
      // const title = item.title.toLowerCase(); There are some items in the JSON file whose material is empty for this case title can be used next to the material
      const material = item.material.toLowerCase();
      if (material.includes(filter)) {
        if (filteredArray.length === 0) {
          filteredArray.push(item);
        } else if (filteredArray.every((same) => same !== item)) {
          filteredArray.push(item);
        }
      }
    });
  };

  // UPDATE RESULTS
  const updateResults = () => {
    results.innerHTML = items.childElementCount + " Results";
  };

  /**---------------------------------------------------    LOAD API    ---------------------------------------------------*/
  const productList = await fetch(
    "https://d2t3o0osqtjkex.cloudfront.net/tgTest/prods.json",
    {
      method: "GET",
      mode: "cors",
    }
  ).then((resp) => resp.json());

  /**---------------------------------------------------    UPDATE DOM   ---------------------------------------------------*/

  // LOAD SAVED OPTIONS FROM SESSION STORAGE
  (() => {
    const options = JSON.parse(sessionStorage.getItem("options"));

    if (windowWidth > 700) columnToggleLabel.style.display = "none";

    if (options) {
      columnToggle.checked = options.column === "checked" ? true : false;
      sortBy.value = options.sort;
      multiple = options.multiple;
      filteredArray = options.filteredArray;

      boxes.map((item) => {
        item.checked = options[item.id] === "checked" ? true : false;
      });
    }

    multiple.length !== 0
      ? priceSorter(filteredArray)
      : priceSorter(productList);

    if (columnToggleLabel.style.display === "none") {
      changeStyle("desktop");
    } else {
      options !== null
        ? changeStyle(options.column === "checked")
        : changeStyle(false);
    }

    updateResults();
  })();
})();
